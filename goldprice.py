#!/usr/bin/python3.7
import os
import requests


payload = {
    'username': os.environ.get('user'),
    'password': os.environ.get('password')
}
headers = {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
    }
cookies = {"__cfduid":"da24d52ea9a505336ea208caaf36c42b41572292258", "has_js":"1", "CookieConsent":"-1", "_ga":"GA1.2.660763886.1572340053", "_gid":"GA1.2.1833310438.1572340053", "__gads":"ID=b92e41c1cd5cc515:T=1572340052:S=ALNI_MbrhHW85gNmrQo0kv03uhRtmmENwg"}

url = "https://data-asg.goldprice.org/dbXRates/INR"

def find_data():
    response = requests.get(url, headers=headers, cookies=cookies)
    data = response.json()
    return data

def login():
    r = requests.post(
        'http://127.0.0.1:8000/api/v1/login',
        data=payload
        )
    data = r.json()
    return data['Token']


def parsing_data():
    conversion = 31.1034768
    headers = {'Authorization': f"Token {login()}"}

    data = find_data()
    items = data['items'][0]
    price_oz = items['xauPrice']
    price_g = f"{(price_oz / conversion):.2f}"
    

    requests.post('http://127.0.0.1:8000/api/v1/rates/gold', {'rate':price_g}, headers=headers)
    return price_g


if __name__=='__main__':
    parsing_data()