#!/usr/bin/sh
import os
import yaml
import requests
import smtplib, ssl
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

# https://myaccount.google.com/lesssecureapps

payload = {
    'username': os.environ.get('user'),
    'password': os.environ.get('password')
}


def get_email_data():
    with open('/home/ec2-user/data_api/config.yaml', 'r') as yaml_file:
        config_yaml = yaml.load(yaml_file)
        sender_email = config_yaml['credential_email']['sender_email']
        password = config_yaml['credential_email']['password']
        return sender_email, password


def login():
    r = requests.post(
        f"{os.environ.get('url')}/api/v1/login",
        data=payload
        )
    if r:
        return r.json().get('Token')

headers = {'Authorization': f"Token {login()}"}


def get_request(url, headers=None):
    r = requests.get(url, headers=headers)
    if r:
        return r.json()


def send_email():
    sender_email, password = get_email_data()
    data = get_request(
        f"{os.environ.get('url')}/api/v1/referrals?mail_sent=False",
        headers=headers
        )
    if data:
        for item in data:
            receiver_email = item['friend_email']
            if receiver_email:
                account_name = get_request(
                    f"{os.environ.get('url')}/api/v1/accounts/{item['account_id']}",
                    headers=headers
                    )['first_name']
                mail_content = """
                    Hello {},\n
                    Your friend {} has been enjoying the
                    mRush game. And wants you to join in on the fun!\n
                    Follow this link to install mRush - A fun Finance 
                    and Stock Market game.
                    Google Play: http://www.google.com\n
                    Apple App Store: http://www.apple.com\n
                    Please ignore this email incase you do not know
                    {}.
                    Happy Investing!\n
                    Team mRush\n""".format(
                        item['friend_first_name'], account_name, account_name
                        )
                message = MIMEMultipart()
                message['From'] = sender_email
                message['To'] = receiver_email
                message['Subject'] = '{} has sent you a gift'.format(account_name)   #The subject line
                #The body and the attachments for the mail
                message.attach(MIMEText(mail_content, 'plain'))
                #Create SMTP session for sending the mail
                session = smtplib.SMTP('smtp.gmail.com', 587) #use gmail with port
                session.starttls() #enable security
                session.login(sender_email, password) #login with mail_id and password
                text = message.as_string()
                session.sendmail(sender_email, receiver_email, text)
                session.quit()
                requests.put(
                    f"{os.environ.get('url')}/api/v1/referrals/{item['id']}",
                    headers=headers
                    )
    return
            
send_email()