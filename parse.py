#!/usr/bin/sh
import os
import requests


conversion = 31.1034768

payload = {
    'username': os.environ.get('user'),
    'password': os.environ.get('password')
}
headers = {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
    }
cookies = {"__cfduid":"da24d52ea9a505336ea208caaf36c42b41572292258", "has_js":"1", "CookieConsent":"-1", "_ga":"GA1.2.660763886.1572340053", "_gid":"GA1.2.1833310438.1572340053", "__gads":"ID=b92e41c1cd5cc515:T=1572340052:S=ALNI_MbrhHW85gNmrQo0kv03uhRtmmENwg"}

url_1 = "https://data-asg.goldprice.org/dbXRates/INR"
url_2 = 'https://data-asg.goldprice.org/dbXRates/USD'

def find_data(url):
    response = requests.get(url, headers=headers, cookies=cookies)
    data = response.json()
    return data

def login():
    r = requests.post(
        f"{os.environ.get('url')}/api/v1/login",
        data=payload
        )
    data = r.json()
    return data['Token']

def get_data(url):
    data = find_data(url)
    items = data['items'][0]
    price = items['xauPrice']
    return price

def parsing_data():
    price_inr_oz = get_data(url_1)    
    price_usd_oz = get_data(url_2)

    requests.post(
        f"{os.environ.get('url')}/api/v1/rates/gold",
        {'rate_inr_gm':f"{(price_inr_oz / conversion):.2f}",
        'rate_usd_oz': f"{price_usd_oz:.2f}"},
        headers={'Authorization': f"Token {login()}"}
        )
    return


if __name__=='__main__':
    parsing_data()