import traceback
import pandas as pd
import json
import re
from os.path import isfile
from time import sleep
from datetime import datetime
from parse import parse

from utils import printf
from viagogo import Viagogo
from gsheets import GSheets

report_spreadsheet = 'Test'
report_worksheet = 'Test'
report_start_col = 1

vgg_login = 'test'
vgg_pass = 'test'

check_delay = 60


def get_report_sales(with_categories=False, return_deleted_sales=False):
    GSheets.authorize()
    report = GSheets.get_spreadsheet(report_spreadsheet, values_by_cols=True, skip_first=True)
    sales_report = report[report_worksheet]
    if sales_report:
        result = None
        sale_ids = sales_report[report_start_col-1]
        
        if with_categories:
            sale_categories = sales_report[report_start_col]
            category_sales = {unique_category: [] for unique_category in set(sale_categories)}
            for i in range(len(sale_ids)):
                category_sales[sale_categories[i]].append(int(sale_ids[i]))
            result = category_sales
        else:
            result = [int(sale_id) for sale_id in sale_ids]
            
        if return_deleted_sales:
            deleted_sales = report['Deleted sales']
            result = result, [int(sale_id) for sale_id in deleted_sales[0]]
        
        return result

def get_formatted_date_pyment(raw_date):
    if '-' in raw_date:
        matches = re.findall('(\d{2}[\/ ](\d{2}|January|Jan|February|Feb|March|Mar|April|Apr|May|May|June|Jun|July|Jul|August|Aug|September|Sep|October|Oct|November|Nov|December|Dec)[\/ ]\d{2,4})', raw_date)
        for match in matches:
            raw_date = match[0]
    try:
        parsed_date = pd.to_datetime(raw_date).strftime('%m/%d/%Y')
    except Exception:
        parsed_date = datetime.strptime(raw_date, '%m/%d/%Y')
    
    return parsed_date
def get_transaction_payments():
    printf('<y>Transaction payments fetching</> <c>started</>')
    payments = Viagogo.get_user_payments()
    # with open('payments.txt', 'w') as file:
        # file.write(json.dumps(payments))
    
    transaction_payments = {}
    cross_cols = ['Proceeds', 'Charges', 'Credit']
    for payment_id, payment_info in payments.items():
        #payment_date = datetime.strptime(payment_info['Date'], '%m/%d/%Y')
        payment_date = get_formatted_date_pyment(payment_info['Date'])
        print('*'*100)
        print(payment_date)
        print('*'*100)
        payment = Viagogo.get_payment_details(payment_id)
        for transaction_id, details in payment.items():
            details['PaymentDate'] = payment_date
            if transaction_id in transaction_payments:
                prev_transaction = transaction_payments[transaction_id]
                for cross_col in cross_cols:
                    prev_transaction[cross_col] += details[cross_col]
                if prev_transaction['PaymentDate'] < details['PaymentDate']:
                    prev_transaction['PaymentDate'] = details['PaymentDate']
            else:
                transaction_payments[transaction_id] = details
        sleep(1)
    printf('<y>Transaction payments fetching</> <g>completed</>')
    
    return transaction_payments


def login_viagogo():
    printf('<y>Login</> <w>[{}]</> <c>started</>'.format(Viagogo.user_login))
    try:
        Viagogo.login()
    except Exception as e:
        printf('<y>Login</> <r>failed</>. <w>Reason:</> <m>{}</>'.format(str(e)))
        Viagogo.reset_session()
        sleep(60)
        raise UserWarning
    printf('<y>Login</> <g>completed</>')

def get_formatted_date(raw_date):
    # In case when we get data range '29 Jun - 06 Jul 2019' we have to split the to first date '29 Jun 2019'
    if '-' in raw_date:
        matches = re.findall('(\d{2}[\/ ](\d{2}|January|Jan|February|Feb|March|Mar|April|Apr|May|May|June|Jun|July|Jul|August|Aug|September|Sep|October|Oct|November|Nov|December|Dec)[\/ ]\d{2,4})', raw_date)
        for match in matches:
            raw_date = match[0]
    try:
        parsed_date = pd.to_datetime(raw_date).strftime('%d.%m.%Y')
    except Exception:
        parsed_date = get_formatted_date_old(raw_date)
    
    return parsed_date
          
def get_formatted_date_old(raw_date):
    # printf(raw_date)
    date_formats = [
        '{m}/{d}/{Y}',
        '{d}/{m}/{Y}',
        '{a} {b} {d} {Y} {H}:{M}-{}:{}',
        '{a} {b} {d} {Y} {H}:{M}-{H}:{M}',
        '{a} {d} {b} {Y} {H}:{M}-{}:{}',
        '{a} {d} {b} {Y} {H}:{M}-{H}:{M}',
        '{a} {b} {d} {Y} {H}:{M}',
        '{a} {d} {b} {Y} {H}:{M}',
        '{A}, {B} {d} {Y}',
        '{A}, {d} {B} {Y}',
        '{b} {d} - {} {} {Y}',
        '{b} {d} - {}, {Y}',
        '{d} {b} - {}, {Y}'   
        '{} - {} {b} {Y}',
        '{d} {b} {Y}',
        '{b} {d} {Y}',
        '{d} {b} - {} {} {Y}',
        '{}, {B} {d} {Y}',
        '{}, {d} {b} {Y}',
        '{} {d} {b} {Y}',
        '{} {b} {d} {Y}'
    ]
    
    for df in date_formats:
        result = parse(df, raw_date)
        if result is not None:
            parsed_date = result.named
            string_date = ' '.join(parsed_date.values())
            date_format = '%' + ' %'.join(parsed_date.keys())
            return datetime.strptime(string_date, date_format).strftime('%d.%m.%Y')
            
    raise Exception('Date of unknown format: ' + raw_date)


last_report_update = datetime.now()
def update_report(new_sales):
    global last_report_update
    printf('<y>Report update</> <c>started</>')
    try:
        transaction_payments = {}
        if datetime.now().day != last_report_update.day:
            transaction_payments = get_transaction_payments()
            for transaction_id, details in transaction_payments.items():
                sale = next((s for s in new_sales if s['Id'] == transaction_id), None)
                if sale is not None:
                    #sale['PaymentDate'] = details['PaymentDate'].strftime('%d.%m.%Y')
                    sale['PaymentDate'] = get_formatted_date(details['PaymentDate'])
                    print('-'*10,'payment','-'*10)
                    print(sale['PaymentDate'])
                    print('-'*10,'payment','-'*10)
                    sale['RealProceeds'] = details['Proceeds'] + details['Charges'] + details['Credit']
                    
        report_sales = get_report_sales()
        not_found_sales = []
        updated_sales = [[] for s in report_sales]
        for new_sale in new_sales:
            new_sale['TransactionDateTime'] = get_formatted_date(new_sale['TransactionDateTime'])
            new_sale['EventDate'] = get_formatted_date(new_sale['EventDate'])
            new_sale['MustShipBy'] = get_formatted_date(new_sale['MustShipBy'])
            sale_cols = [
                'TransactionId',
                'Status',
                'TransactionDateTime',
                'EventId',
                'EventName',
                'EventDate',
                'VenueCountry',
                'VenueCity',
                'VenueName',
                'Section',
                'Quantity',
                'ProceedsVal',
                'CostVal',
                'MustShipBy',
                'PaymentDate',
                'RealProceeds'
            ]
            if new_sale['Id'] in report_sales:
                sale_update_cols = ['Status', 'MustShipBy', 'PaymentDate', 'RealProceeds']
                sale_vals = []
                for sale_col in sale_cols:
                    if sale_col in sale_update_cols:
                        sale_vals.append(new_sale[sale_col])
                    else:
                        sale_vals.append(None)
                old_sale_index = report_sales.index(new_sale['Id'])
                updated_sales[old_sale_index] = sale_vals
            else:
                sale_vals = [new_sale[key] for key in sale_cols]
                not_found_sales.append(sale_vals)
            #printf('<y>Sale</> <w>[{}]</> <g>recorded</>'.format(new_sale['Id']))
        if any(sale for sale in updated_sales):
            GSheets.update_values(report_spreadsheet, report_worksheet, updated_sales, report_start_col, start_row=2)
            printf('<y>Old sales</> <g>updated</>')
        if not_found_sales:
            GSheets.append_values(report_spreadsheet, report_worksheet, not_found_sales, report_start_col)
            printf('<y>New sales</> <g>added</>')
        last_report_update = datetime.now()
        printf('<y>Report update</> <g>completed</>')
    except Exception as e:
        printf('<y>Report update</> <r>failed</>. <w>Reason:</> <m>{}</>'.format(str(e)))
        printf(traceback.format_exc())
        sleep(60)
        raise UserWarning


def check_sales():    
    printf('<y>Sales fetching</> <c>started</>')
    try:
        new_sales = []
        vgg_sales = Viagogo.get_sales()
        for sales_category, transactions in vgg_sales.items():
            transaction_ids = [t['Id'] for t in transactions]
            category_transaction_ids, deleted_sales = get_report_sales(with_categories=True, return_deleted_sales=True)
            if sales_category in category_transaction_ids:
                new_category_transactions = list(set(transaction_ids)-set(category_transaction_ids[sales_category]))
            else:
                new_category_transactions = transaction_ids
            new_category_sales = [t for t in transactions if t['Id'] in new_category_transactions]
            for sale in new_category_sales:
                if sale['Id'] not in deleted_sales:
                    try:
                        sale_details = Viagogo.get_sale_details(sale['Id'])
                        user_listings = Viagogo.get_user_listings(sale['EventId'])
                        user_listing = next(listing for listing in user_listings if listing['Id'] == sale_details['ListingId'])
                        cost_value = int(user_listing['FaceValueVal'] * sale['Quantity'])
                        sale['CostVal'] = cost_value
                    except:
                        sale['CostVal'] = None
                    sale['Status'] = sales_category
                    sale['PaymentDate'] = None
                    sale['RealProceeds'] = None
                    new_sales.append(sale)
                    
                    printf('<y>New sale</> <w>[{}\{}]</> <c>found</>'.format(sales_category, sale['Id']))
                    sleep(3)
            for sale in transactions:
                if sale['Id'] not in deleted_sales and sale['Id'] not in new_category_transactions:
                    sale['CostVal'] = None
                    sale['Status'] = None
                    sale['PaymentDate'] = None
                    sale['RealProceeds'] = None
                    new_sales.append(sale)
            
        printf('<y>Sales fetching</> <g>completed</>')
    except Exception as e:
        printf('<y>Sales fetching</> <r>failed</>. <w>Reason:</> <m>{}</>'.format(str(e)))
        printf(traceback.format_exc())
        Viagogo.reset_session()
        sleep(60)
        raise UserWarning
        
    if new_sales: update_report(new_sales)


printf('<y>Viagogo sales reporter</> <w>[1.0]</> <c>started</>')
Viagogo.set_user(vgg_login, vgg_pass)
GSheets.set_sa_credentials('credentials.json')

while True:
    try:
        login_viagogo()
        check_sales()
    except UserWarning:
        pass
    except Exception as e:
        printf('<r>Unhandled exception:</> <m>{}</>'.format(str(e)))
        
    printf('<b>Sleep</> <w>[{}m]</>'.format(check_delay))
    sleep(60*check_delay)
